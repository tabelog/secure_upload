module SecureUpload
  module Scanners
    class Base
      def initialize(options = {})
        @options = default_options.merge(options)
      end

      def scan(path)
        # Scan the file here
        true
      end

      private

      def default_options
        {}
      end

      def options
        @options
      end

      def logger
        @logger ||= SecureUpload.configuration.logger
      end
    end
  end
end
