module SecureUpload
  module Scanners
    class Fsecure < Base
      def scan(path)
        now_umask = File.umask(0)
        output = ''
        File.open(options[:lockfile_path], 'w', 0666) do |f| 
          f.flock(File::LOCK_EX)
          begin
            output = `#{options[:bin_path]} #{path}`
          ensure
            f.flock(File::LOCK_UN)
          end 
        end 
        result = File.exist?(path)
        if result
          logger.info <<-EOS
Excecute: #{options[:bin_path]} #{path}
Scanned:
#{output}
          EOS
        else
          logger.warn <<-EOS
Excecute: #{options[:bin_path]} #{path}
Detected:
#{output}
          EOS
        end 
        result
      ensure
        File.umask(now_umask)
        false
      end 

      private

      def default_options
        {
          :bin_path => "/usr/bin/fsav",
          :lockfile_path => "/tmp/fsav_lock"
        }
      end
    end 
  end 
end
