require 'secure_upload'
require 'secure_upload/utility'
module SecureUpload
  class UnsecureFileError < StandardError
    def initialize(msg = "The uploaded file(s) are unsecure."); super; end 
  end 

  class Middleware
    include Utility

    def initialize(app)
      @app = app 
    end 

    def call(env)
      params = Rack::Multipart.parse_multipart(env)
      if params && !params.empty?
        traverse(params) do |value|
          if [Tempfile].any?{|klass|value.is_a?(klass)}
            raise UnsecureFileError if scanner.scan(value.path) == false
          end 
        end 
      end 

      @app.call(env)
    end

    private

    def scanner
      @scanner ||= SecureUpload.configuration.scanner
    end
  end
end
