require 'rails'
require 'secure_upload'
require 'secure_upload/middleware'
module SecureUpload
  class Railtie < ::Rails::Railtie
    initializer "secure_upload.railtie.initializer" do |app|
      app.middleware.use SecureUpload::Middleware
      SecureUpload.configuration do |c|
        c.logger = Rails.logger
      end
    end
  end
end
