require 'logger'
require 'secure_upload/scanners'
module SecureUpload
  class Configuration
    # The logger to use when logging scanner's output. If you
    # are using Secure Upload with Rails, you do not need to set this attribute manually.
    # If you are not using it with Rails and not set this attribute, nothing will be logged.
    # 
    # @return [Logger]
    attr_writer :logger

    def scanner
      @scanner ||= SecureUpload::Scanners::Base.new
    end

    # @return [SecureUpload::Scanners::Base]
    attr_writer :scanner

    def logger
      @logger ||= ::Logger.new('/dev/null')
    end
  end
end
