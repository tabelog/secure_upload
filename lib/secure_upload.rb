# encoding: utf-8

require 'rack'
require 'secure_upload/configuration'
require 'secure_upload/middleware'
require "secure_upload/railtie" if defined?(::Rails)
module SecureUpload
  def self.configuration
    @configuration ||= Configuration.new
  end
  
  def self.configure
    yield configuration
  end
end
