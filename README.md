secure_upload
=============

Upload files securely

Installation
------------

Install the gem:

```ruby
gem install secure_upload
```

Or even better, add it to your Gemfile.

```ruby
source "https://rubygems.org"
gem 'secure_upload'
```

Contributing
------------

Checkout out CONTRIBUTING for more info.
