#!/usr/bin/env ruby

module MockFsecure
  extend self

  def clean(path)
    data = open(path).read.strip
    if data == malicious_data
      File.delete path
      exit 1
    else
      exit 0
    end
  end

  def bin_path
    __FILE__
  end

  def malicious_data
    'malicious'
  end
end

if __FILE__ == $0
  MockFsecure.clean(ARGV[0])
end

