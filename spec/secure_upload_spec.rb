require "spec_helper"

describe SecureUpload do
  it "should return configuration instance for configuration method" do
    SecureUpload.configuration.is_a?(SecureUpload::Configuration).should be_true
  end

  it "should yield the procedure with configuration argument" do
    something = ""
    something.should_receive :downcase
    SecureUpload.configure do |c|
      c.is_a?(SecureUpload::Configuration).should be_true
      something.downcase
    end
  end
end
