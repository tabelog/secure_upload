require 'rubygems'
require 'bundler/setup'

require 'secure_upload'

RSpec.configure do |config|

end

def multipart_fixture(data)
  boundary = "AaB03x"
  type = "multipart/form-data; boundary=#{boundary}"
  input = <<-EOS
--#{boundary}
Content-Disposition: form-data; name="text"
Content-Type: text/plain; charset=UTF-8

#{data}
--#{boundary}--
EOS
  length = input.respond_to?(:bytesize) ? input.bytesize : input.size
  { "CONTENT_TYPE" => type,
    "CONTENT_LENGTH" => length.to_s,
    :input => StringIO.new(input) }
end

