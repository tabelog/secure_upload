require "spec_helper"

module SecureUpload
  describe Middleware do
    let(:app) { Middleware.new(->env { ":)" }) }

    it "should pass non-error responses through" do
      env = Rack::MockRequest.env_for('/')
      app.call(env).should == ":)"
    end

    it "should have scanner" do
      app.private_methods.include?(:scanner).should be_true
      app.send(:scanner).is_a?(SecureUpload::Scanners::Base).should be_true
    end

    context "with uploaded file" do
      it "should scan the file" do
        app.send(:scanner).should_receive :scan
        files = Rack::Multipart::UploadedFile.new(__FILE__)
        env = Rack::MockRequest.env_for('/', :method => :post, :params => {:something => files})
        app.call(env)
      end

      it "should throw exception when scanner return false" do
        env = Rack::MockRequest.env_for('/')
        files = Rack::Multipart::UploadedFile.new(__FILE__)
        env = Rack::MockRequest.env_for('/', :method => :post, :params => {:something => files})
        app.send(:scanner).stub(:scan) do
          false
        end
        expect {
          app.call(env)
        }.to raise_error SecureUpload::UnsecureFileError
      end
    end

    context "without uplaod file" do
      it "should not call scan with no parameter" do
        app.send(:scanner).should_not_receive :scan
        env = Rack::MockRequest.env_for('/')
        app.call(env)
      end
    end
  end
end
