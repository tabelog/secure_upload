require "spec_helper"

module SecureUpload
  describe Configuration do
    let(:configuration) {SecureUpload::Configuration.new}

    it "should have scanner accessor with default" do
      configuration.scanner.is_a?(SecureUpload::Scanners::Base).should be_true
      configuration.scanner = 'scanner'
      configuration.scanner.should == 'scanner'
    end

    it "should have logger accessor with default" do
      configuration.logger.is_a?(::Logger).should be_true
      configuration.logger = 'logger'
      configuration.logger.should == 'logger'
    end
  end
end
