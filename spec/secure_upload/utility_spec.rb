require "spec_helper"

module SecureUpload
  describe Utility do
    include Utility
    
    context 'traverse method' do
      it "should yield the block for each values of array" do
        targets = [1, 2, 3]
        checked = []
        traverse(targets) do |v|
          checked << v
        end
        checked.should == targets
      end

      it "should yield the block for each values of hash" do
        targets = {:a => 1, :b => 2, :c => 3}
        checked = Set.new
        traverse(targets) do |v|
          checked << v
        end
        checked.should == Set.new(targets.keys + targets.values)
      end

      it "should yield the block for each values of complicated hash" do
        targets = {:a => [1, 2, 3], :b => {'x' => [0], 'y' => 'Y'}, :c => 3}
        checked = Set.new
        traverse(targets) do |v|
          checked << v
        end
        checked.should == Set.new([:a, 1, 2, 3, :b, 'x', 0, 'y', 'Y', :c, 3])
      end
    end
  end
end
