require "spec_helper"

module SecureUpload
  module Scanners
    describe Base do
      let(:scanner) {Base.new(:something => true)}

      it "should always return true when scan a file" do
        res = scanner.scan('')
        res.should be_true
      end

      it "should have private options method" do
        scanner.private_methods.include?(:options).should be_true
        scanner.send(:options).should == {:something => true}
      end

      it "should have private logger method" do
        scanner.private_methods.include?(:logger).should be_true
        scanner.send(:logger).is_a?(::Logger).should be_true
      end

      it "should return merged options if inherited class override default_options method" do
        class Something < Base
          def default_options
            {:other_thing => true}
          end
        end
        scanner = Something.new(:something => true)
        scanner.send(:options).should == {:something => true, :other_thing => true}
      end
    end
  end
end
