require "spec_helper"
require 'mock_fsecure'

module SecureUpload
  module Scanners
    describe Fsecure do
      let(:scanner) { Fsecure.new(:bin_path => MockFsecure.bin_path) }

      it "should return false when scan malicious file" do
        temp = Tempfile.new('secure_upload')
        temp.write(MockFsecure.malicious_data)
        temp.flush
        res = scanner.scan(temp.path)
        res.should be_false
      end

      it "should return true when scan normal file" do
        temp = Tempfile.new('secure_upload')
        temp.write('something')
        temp.flush
        res = scanner.scan(temp.path)
        res.should be_true
      end
    end
  end
end
