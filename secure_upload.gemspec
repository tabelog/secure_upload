# -*- encoding: utf-8 -*-
require File.expand_path('../lib/secure_upload/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors     = [ 'Daisuke Taniwaki' ]
  gem.email       = [ 'daisuketaniwaki@gmail.com' ]
  gem.summary     = "Upload files securely"
  gem.description = gem.summary
  gem.homepage    = "http://github.com/dtaniwaki/secure_upload"

  gem.files            = `git ls-files`.split("\n")
  gem.test_files       = `git ls-files -- {spec}/*`.split("\n")
  gem.extra_rdoc_files = %w[]

  gem.name          = "secure_upload"
  gem.require_paths = [ "lib" ]
  gem.version       = SecureUpload::VERSION

  gem.add_runtime_dependency 'logger', '>= 1.2'
  gem.add_runtime_dependency 'rack', '>= 1.4'

  gem.add_development_dependency 'rake', '~> 10.0'
  gem.add_development_dependency 'rspec', '~> 2.12'
end
